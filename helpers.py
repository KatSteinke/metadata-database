"""Assorted helper functions."""

__author__ = "Kat Steinke"

import re

import pipeline_config

default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF


# copied from run_pipeline from Corona pipeline -
# TODO: how to manage this? May make this more universal - just import the whole thing?


def map_sample_number_to_letter(sample_hit: re.Match) -> str:
    """Replace numbers in beginning of sample number with corresponding letters."""
    value_found = sample_hit.group(0)
    if value_found in workflow_config["sample_number_settings"]["number_to_letter"]:
        return workflow_config["sample_number_settings"]["number_to_letter"][value_found]

    return value_found


def map_sample_letter_to_number(sample_hit: re.Match) -> str:
    """Replace letters in beginning of sample number with corresponding numbers."""
    # start by reversing number_to_letter
    letter_to_number = {letter: number
                        for (number, letter) in workflow_config["sample_number_settings"]["number_to_letter"].items()}
    value_found = sample_hit.group(0)
    if value_found in letter_to_number:
        return letter_to_number[value_found]

    return value_found
