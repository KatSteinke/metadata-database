"""Parse nanopore runsheets."""

__author__ = "Kat Steinke"
import datetime
import pathlib
import re
import warnings

from argparse import ArgumentParser

import numpy as np
import pandas as pd
import yaml

from check_config import check_config
from helpers import map_sample_number_to_letter
import pipeline_config

default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

# disable the warning about DataValidation as it won't impact the data
# (compare https://stackoverflow.com/a/66571471/15704972)
warnings.filterwarnings('ignore',
                        message="Data Validation extension is not supported and will be removed",
                        module="openpyxl")

def validate_runsheet_format(runsheet: pathlib.Path) -> None:
    """Identify wrong sample name or barcode formats in runsheet.
    Arguments:
        runsheet:   Path to runsheet to check
    """
    # check if runsheet was formatted correctly, throw error and print everything that's wrong in one go
    # get only the relevant columns here: KMA nr, Ct værdi, barkode; skip the first three lines
    # (not usable for parsing)
    sheet_data = pd.read_excel(runsheet, usecols="A:C", skiprows=3, dtype={"KMA nr": str,
                                                                           "Barkode NB": str})
    sheet_issues = False
    data_missing = False
    # set up record of issues so they can all be printed at once
    fail_record = "The following issue(s) were detected with the runsheet:"
    # check that sample numbers and barcodes have been entered, fail early if so
    no_sample_ids = sheet_data["KMA nr"].isna().all()
    if no_sample_ids:
        fail_record += "\nNo sample IDs found."
        data_missing = True
    no_barcodes = sheet_data["Barkode NB"].isna().all()
    if no_barcodes:
        fail_record += "\nNo barcodes found."
        data_missing = True
    if data_missing:
        raise ValueError(fail_record)
    # now we can be sure there are sample IDs and barcodes, we can check them
    # start by checking if we have the same amount of sample IDs and barcodes
    amount_sample_ids = sheet_data["KMA nr"].dropna().size
    amount_barcodes = sheet_data["Barkode NB"].dropna().size
    if amount_sample_ids != amount_barcodes:
        sheet_issues = True
        fail_record += f"\nAmount of sample IDs and barcodes don't match. " \
                       f"There are {amount_sample_ids} sample IDs but {amount_barcodes} barcodes."
        # check that positive and negative controls are included
        # for positive controls: see if there are any sample numbers matching the controls
    # check controls
    positive_controls_in_sheet = set(workflow_config["sample_number_settings"]["positive_control"]).intersection(
        sheet_data["KMA nr"])
    if not positive_controls_in_sheet:
        sheet_issues = True
        fail_record += "\nNo positive controls given in runsheet."
    # for negative controls: see if there is anything matching negative control pattern
    negative_controls_in_sheet = sheet_data["KMA nr"].str.fullmatch(
        workflow_config["sample_number_settings"]["negative_control"],
        na=False)
    if not negative_controls_in_sheet.any():
        sheet_issues = True
        fail_record += "\nNo negative controls given in runsheet."
    # positive control format has been checked, remove positive controls before checking the rest
    # TODO: could be more explicit by compiling positive control list as pattern but likely at the cost of speed
    sheet_data = sheet_data.loc[
        ~sheet_data["KMA nr"].isin(workflow_config["sample_number_settings"]["positive_control"])]
    # TODO: represent as raw string?
    id_pattern = '^(' \
                 + workflow_config["sample_number_settings"]["sample_number_format"] \
                 + '|' \
                 + workflow_config["sample_number_settings"]["negative_control"] \
                 + ')$'
    fail_ids = sheet_data["KMA nr"][~sheet_data["KMA nr"].apply(str).str.match(id_pattern, na=False)].dropna().tolist()
    if fail_ids:
        sheet_issues = True
        # adapt error message to sample number format - TODO: do we need a second sanity check here
        if workflow_config['sample_number_settings']['sample_numbers_in'] == "number":
            allowed_start = workflow_config['sample_number_settings']['number_to_letter'].keys()
        else:
            allowed_start = workflow_config['sample_number_settings']['number_to_letter'].values()
        # TODO: make sample number length variable?
        fail_record += f"\nSample IDs {fail_ids} are not valid. " \
                       f"Sample IDs must start with {' or '.join(allowed_start)} followed by " \
                       f"eight numbers (six if leaving out year). " \
                       f"Negative controls must be given in the format " \
                       f"{workflow_config['sample_number_settings']['negative_control']}. " \
                       f"Please correct sample IDs in runsheet."

    # check that Ct value doesn't have anything that's not a number
    # ct_format = sheet_data["CT/CP værdi"]
    # check that barcodes have correct format: use negative lookbehind to find all lines not matching the format
    fail_barcodes = sheet_data["Barkode NB"][
        ~sheet_data["Barkode NB"].apply(str).str.match(workflow_config["barcode_format"],
                                                       na=False)].dropna().tolist()
    if fail_barcodes:
        sheet_issues = True
        # here we append to the record of issues
        fail_record += f"\nBarcodes {fail_barcodes} are not valid barcodes. " \
                       f"Barcodes must consist of {workflow_config['barcode_prefix']} " \
                       f"+ a number between 01 and 96."
    # duplicated sample numbers have been checked in the separate runsheet check
    # duplicated barcodes indicate a serious issue though
    if any(sheet_data["Barkode NB"].dropna().duplicated()):
        sheet_issues = True
        duplicated_barcodes = sheet_data["Barkode NB"][sheet_data["Barkode NB"].duplicated()].dropna().unique()
        fail_record += f"\nBarcode(s) {duplicated_barcodes} are duplicated."
    if sheet_issues:
        raise ValueError(fail_record)


def fix_ids_by_prefix(runsheet_data: pd.DataFrame, lab_data: pd.DataFrame, sheet_prefix: str,
                      lab_data_prefix: str) -> pd.DataFrame:
    """Fix IDs in runsheet (by identifying and adding year) for a given sample type (indicated by
    sample number prefixes).
    Arguments:
        runsheet_data:      Data contained in runsheet
        lab_data:           Data contained in laboratory information system report
                            (minimum: sample numbers and date received)
        sheet_prefix:       Sample number prefix designating desired sample type in sample sheet
        lab_data_prefix:    Sample number prefix corresponding to sheet_prefix in the format used in
                            the LIS report
    Returns:
        A dataframe with sample ID, barcodes and Ct value from the runsheet, with the year
        identified from the LIS report and added to the sample ID correspondingly.
    """
    # reduce runsheet to only the relevant samples - make a copy to ensure the original stays
    # unchanged
    runsheet_filtered = runsheet_data[runsheet_data["proevenr"].str.startswith(sheet_prefix)].copy()
    # check if this leaves us with any data
    if runsheet_filtered.empty:
        raise ValueError(f"No samples with prefix {sheet_prefix} found in runsheet.")
    # piece sample number together: get the prefix, identify the year, then add the last six
    # characters
    # prefix is already known
    runsheet_filtered["proevenr_prefix"] = sheet_prefix
    # extract last six characters
    runsheet_filtered["proevenr_kort"] = runsheet_filtered["proevenr"].str.slice(start=-6)
    # to find year, check lab information system data and go for date *received*
    # extract relevant samples again
    lab_data_filtered = lab_data[lab_data["prøvenr"].str.startswith(lab_data_prefix)].copy()
    # get last six characters of sample number to match the one from the runsheet
    lab_data_filtered["proevenr_kort"] = lab_data_filtered["prøvenr"].str.slice(start=-6)
    # check if there are mismatches between runsheet and MADS data
    missing_from_mads = runsheet_filtered[~runsheet_filtered["proevenr_kort"].isin(lab_data_filtered["proevenr_kort"])]["proevenr"].dropna().tolist()
    if missing_from_mads:
        raise ValueError(f"Samples {missing_from_mads} were not found in MADS report. "
                         f"Please check that sample numbers are correct.")
    runsheet_filtered = runsheet_filtered.merge(lab_data_filtered, on="proevenr_kort", how="left")
    runsheet_filtered["modtaget"] = runsheet_filtered["modtaget"].apply(lambda x:
                                                                        datetime.datetime.strptime(str(x),
                                                                                                   "%d%m%Y").date()
                                                                        if pd.notna(x)
                                                                        else x)
    runsheet_filtered["år_modtaget"] = runsheet_filtered["modtaget"].apply(lambda x: x.strftime("%y")
                                                                           if pd.notna(x)
                                                                           else "")
    runsheet_filtered["proevenr"] = runsheet_filtered["proevenr_prefix"] \
                                    + runsheet_filtered["år_modtaget"] \
                                    + runsheet_filtered["proevenr_kort"]
    return runsheet_filtered[["proevenr", "barcode"]]


def create_metadata_from_runsheet(runsheet: pathlib.Path, rawdata: pathlib.Path,
                                  sequencing_protocol: str,
                                  sampletype: str, sequencing_platform: str,
                                  lab_info_report: pathlib.Path) -> pd.DataFrame:
    """Read a runsheet used for SARS-CoV-2 sequencing on nanopore
    and extract metadata from it.
    Arguments:
        runsheet:               Path to runsheet to extract metadata from
        rawdata:                Path to directory containing raw sequencing data
        sequencing_protocol:    Sequencing protocol used for sequencing run
        sampletype:            Type of sample (virus isolate, bacterial isolate, WGS...)
        sequencing_platform:               Sequencing platform
        lab_info_report:        Path to report from laboratory information system containing
                                sample numbers and date received
    Returns:
          Metadata to be recorded in the database
    """
    # check the header first
    # relevant dates, run name and initials are contained in header rows - get these first ->
    # fail early for an old runsheet
    run_data = pd.read_excel(runsheet, nrows = 2, skiprows = 1, usecols = "A:D")
    # check that we don't have the old layout
    if "Start dato og RUNxxxx-navn (Præ-PCR)" in run_data.columns.values \
            or "Slut dato, flowcelle og navn (Post-PCR)" in run_data.columns.values:
        raise ValueError("Old runsheet layout detected. Please use a runsheet with the new layout.")
    # split on space, extract the respective field
    # TODO: make prettier!
    run_name = run_data.loc[0, "RUNxxxx-INI (præ-PCR)"]
    postpcr_date = run_data.loc[0, "Dato for post-PCR (YYYY-MM-DD)"]
    labtech_initials = run_data.loc[0, "Initialer post-PCR"]
    # sanity check dates - if we have it formatted as a date we just need to return it as such
    if isinstance(postpcr_date, datetime.datetime):
        postpcr_date = postpcr_date.strftime("%Y-%m-%d")
    else:
        if not re.fullmatch(r'\d{4}-\d{2}-\d{2}', str(postpcr_date)):
            raise ValueError(
                f"Post-PCR date {str(postpcr_date)} has the wrong format. "
                f"Please enter date as YYYY-MM-DD.")

    # sanity check format - validate runsheet
    validate_runsheet_format(runsheet)
    # sample number and barcode can be extracted from "main" rows of runsheet
    metadata = pd.read_excel(runsheet, usecols="A:C", skiprows=3, dtype={"KMA nr": str,
                                                                         "Barkode NB": str})
    metadata = metadata.rename(columns={"KMA nr": "proevenr", "Barkode NB": "barcode"})
    # drop any NA here
    metadata = metadata.dropna()
    # translate sample numbers for use with MADS results
    lab_info_data = pd.read_csv(lab_info_report, encoding="latin1", dtype={"afsendt": str,
                                                                           "cprnr.": str,
                                                                           "modtaget": str})
    errors = []
    sheets = []
    prefix_mapping = workflow_config["sample_number_settings"]["number_to_letter"]
    all_prefixes = metadata["proevenr"].apply(lambda x: x[:2]
    if not (re.match(workflow_config["sample_number_settings"]["negative_control"], x)
            or x in workflow_config["sample_number_settings"]["positive_control"])
    else pd.NA)
    unique_prefixes = all_prefixes.dropna().unique()
    for prefix in unique_prefixes:
        try:
            sheet_data_for_prefix = fix_ids_by_prefix(metadata, lab_info_data, prefix,
                                                      prefix_mapping[prefix])
            sheets.append(sheet_data_for_prefix)
        except ValueError as value_err:
            errors.append(value_err)

    if errors:
        print("The following issues were encountered:")
        error_text ="\n".join([str(parsing_error) for parsing_error in errors])
        raise ValueError(error_text)
    sheet_data_negk = metadata[metadata["proevenr"].str.match(workflow_config["sample_number_settings"]["negative_control"])]
    sheet_data_posk = metadata[metadata["proevenr"].isin(workflow_config["sample_number_settings"]["positive_control"])]
    sheets.extend([sheet_data_negk, sheet_data_posk])
    metadata = pd.concat(sheets, ignore_index=True)
    start_options = "|".join(workflow_config["sample_number_settings"]["number_to_letter"].keys())
    start_pattern = f"^({start_options})"
    metadata["proevenr"] = metadata["proevenr"].apply(lambda x: re.sub(start_pattern,
                                                                       map_sample_number_to_letter,
                                                                       x))

    # now add everything that is universal for the whole run
    metadata["koersel"] = run_name
    metadata["initialer_start"] = labtech_initials
    metadata["sekventeringsdato"] = postpcr_date
    # flowcell is in reagent list
    reagents = pd.read_excel(runsheet, skiprows=2, usecols="M:N", index_col=0, names=["Reagenser",
                                                                                      "Lot nr"])
    flowcell_name = reagents.loc["Flowcelle", "Lot nr"]
    metadata["flowcelle"] = flowcell_name
    # rawdata path needs to be passed in - TODO: by sample, find barcode
    # (can be taken from Snakemake config if we use this as submodule)
    metadata["outdata"] = str(rawdata.resolve())
    # runsheet path was supplied to the function anyway
    metadata["runsheet"] = str(runsheet.resolve())
    # platform, owner, type are constant for now
    metadata["platform"] = sequencing_platform
    metadata["ejer"] = "KMA"
    metadata["protokol"] = sequencing_protocol
    metadata["undersoegelsestype"] = sampletype
    # isolate number does not apply
    metadata["isolatnr"] = np.nan
    return metadata[["proevenr", "barcode", "isolatnr", "koersel", "undersoegelsestype", "ejer",
                     "sekventeringsdato", "initialer_start", "platform", "protokol", "flowcelle",
                     "runsheet", "outdata"]]


# *append* to spreadsheet:
# https://stackoverflow.com/questions/47737220/append-dataframe-to-excel-with-pandas?
if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Extract metadata from runsheet and append to "
                                            "metadata file")
    arg_parser.add_argument("runsheet", help="Path to SARS-CoV-2 runsheet")
    arg_parser.add_argument("sequencing_data", help="Path to folder with sequencing raw data")
    arg_parser.add_argument("database_file", help="Spreadsheet to append results to")
    arg_parser.add_argument("lab_database", help="Path to LIS report")
    # TODO: improve - maintain list of sequencing types and protocols and check if matches
    arg_parser.add_argument("--sample_type",
                            help="Type of sample sequenced in sequencing run (e.g virus-isolat)",
                            default="virus-isolat")
    arg_parser.add_argument("--platform", help="Sequencing platform (default: GridION)",
                            default=workflow_config["platform"])
    arg_parser.add_argument("--protocol",
                            help="Sequencing protocol used for sequencing run (e.g. V4)",
                            default=f"{workflow_config['sequencing_protocol']['default']}_"
                                    f"primer_{workflow_config['primer_version']}")
    arg_parser.add_argument("--config_file", help="Configuration file for workflow")
    args = arg_parser.parse_args()
    run_sheet = pathlib.Path(args.runsheet)
    sequencing_data = pathlib.Path(args.sequencing_data)
    metadata_file = pathlib.Path(args.database_file)
    lis_data = pathlib.Path(args.lab_database)
    sample_type = args.sample_type
    protocol = args.protocol
    if args.config_file:
        workflow_config_file = pathlib.Path(args.config_file)
        with open(workflow_config_file, "r", encoding = "utf-8") as new_config:
            workflow_config = yaml.safe_load(new_config)
        check_config(workflow_config)
    # platform can be overridden manually
    if not args.platform:
        platform = workflow_config["platform"]
    else:
        platform = args.platform
    # check if everything exists
    if not run_sheet.exists():
        raise FileNotFoundError("Runsheet not found.")
    if not sequencing_data.exists():
        raise FileNotFoundError("Directory with sequencing data not found.")
    if not metadata_file.exists():
        raise FileNotFoundError("Metadata spreadsheet not found.")
    if not lis_data.exists():
        raise FileNotFoundError("MADS data not found")
    # for now: load the entire thing into memory and overwrite - TODO: improve
    metadata_old = pd.read_excel(metadata_file)
    metadata_from_runsheet = create_metadata_from_runsheet(run_sheet, sequencing_data, protocol,
                                                           sample_type, platform,
                                                           lis_data)
    metadata_merged = pd.concat([metadata_old, metadata_from_runsheet], ignore_index=True)
    metadata_merged.to_excel(metadata_file, index=False)
