"""Package version
Version 2.0.0: allow Illumina runsheets; use universal config
Version 2.0.1: change coding of primer schemes
"""

__version__ = "2.0.1"
