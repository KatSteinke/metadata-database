"""Check that settings specified in config file are valid."""

from typing import Dict


def check_config(workflow_config: Dict) -> None:
    """Check config file for invalid settings and raise an error for all at once.
    Arguments:
        workflow_config:    Parsed configuration taken from workflow's config file
    """
    permitted_primers =  {"3", "4", "4.1", "1200", "1200.3"}
    # collect errors and return everything at once
    error_message = "The following issue(s) were detected with the config file:"
    fail_record = []
    if not workflow_config["sample_number_settings"]["sample_numbers_in"] in {"number", "letter"}:
        fail_record.append("Format of sample numbers used as input must be given as 'number' or 'letter'.")
    if not workflow_config["sample_number_settings"]["sample_numbers_out"] in {"number", "letter"}:
        fail_record.append("Desired format of sample numbers in output must be given as 'number' or 'letter'.")
    if not workflow_config["primer_version"] in permitted_primers:
        fail_record.append(f"Invalid primer scheme {workflow_config['primer_version']}."
                           f" Primer scheme must be one of {sorted(permitted_primers)}.")
    if fail_record:
        raise ValueError(error_message + "\n" + "\n".join(fail_record))
