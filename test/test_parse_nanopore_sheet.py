import pathlib
import re
import unittest

import numpy as np
import pandas as pd
import pytest

import parse_nanopore_runsheet

class TestValidateRunsheet(unittest.TestCase):
    def test_fail_ids(self):
        id_fail_sheet = pathlib.Path(__file__).parent /"data"/ "utilities_test" / "runsheet-id-fail.xlsx"
        error_msg = "The following issue(s) were detected with the runsheet:"
        error_msg += "\nSample IDs ['123'] are not valid. Sample IDs must start with 70 or 40 " \
                     "followed by eight numbers (six if leaving out year). " \
                     "Negative controls must be given in the format (NegK[0-9]*|(70)?180000(05|1[0-3])). " \
                     "Please correct sample IDs in runsheet."
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            parse_nanopore_runsheet.validate_runsheet_format(id_fail_sheet)

    def test_fail_barcodes(self):
        barcode_fail_sheet = pathlib.Path(__file__).parent /"data" /"utilities_test" / "runsheet-barcode-fail.xlsx"
        error_msg = "The following issue(s) were detected with the runsheet:"
        error_msg += "\nBarcodes ['3'] are not valid barcodes. " \
                     "Barcodes must consist of NB + a number between 01 and 96."
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            parse_nanopore_runsheet.validate_runsheet_format(barcode_fail_sheet)

    def test_fail_no_ids(self):
        no_id_sheet = pathlib.Path(__file__).parent / "data" /"utilities_test" / "runsheet-no-id.xlsx"
        error_msg = "The following issue(s) were detected with the runsheet:"
        error_msg += "\nNo sample IDs found."
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            parse_nanopore_runsheet.validate_runsheet_format(no_id_sheet)

    def test_fail_no_barcodes(self):
        no_barcode_sheet = pathlib.Path(__file__).parent / "data" /"utilities_test" / "runsheet-no-barcode.xlsx"
        error_msg = "The following issue(s) were detected with the runsheet:"
        error_msg += "\nNo barcodes found."
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            parse_nanopore_runsheet.validate_runsheet_format(no_barcode_sheet)


class TestReadRunsheet(unittest.TestCase):
    success_runsheet_path = pathlib.Path(__file__).parent / "data" / "sars_runsheet" / "correct_runsheet.xlsx"
    rawdata_path =  pathlib.Path(__file__).parent / "data" / "sars_runsheet" / "rawdata_dir"
    fake_mads = pathlib.Path(__file__).parent / "data" / "sars_runsheet" / "fake_mads_data.csv"
    def test_fail_old_sheet(self):
        fail_runsheet_path =  pathlib.Path(__file__).parent / "data" / "sars_runsheet" / "old_runsheet.xlsx"
        with pytest.raises(ValueError, match="Old runsheet layout detected. Please use a runsheet with the new layout."):
            parse_nanopore_runsheet.create_metadata_from_runsheet(fail_runsheet_path, self.rawdata_path, "V4",
                                                                  "virus-isolat", "MinION", self.fake_mads)

    def test_fail_date(self):
        fail_runsheet_path = pathlib.Path(__file__).parent / "data" / "sars_runsheet" / "fail_date_runsheet.xlsx"
        with pytest.raises(ValueError,
                           match="Post-PCR date 20211101 has the wrong format. Please enter date as YYYY-MM-DD."):
            parse_nanopore_runsheet.create_metadata_from_runsheet(fail_runsheet_path, self.rawdata_path, "V4",
                                                                  "virus-isolat", "MinION", self.fake_mads)

    def test_parse_sheet(self):
        true_result = pd.DataFrame(data={"proevenr": ["P21000000", "P21000001", "NegK", "18000006"],
                                         "barcode": ["NB01", "NB02", "NB04", "NB03"],
                                         "isolatnr": [np.nan, np.nan, np.nan, np.nan],
                                         "koersel": ["RUN0001-XYZ", "RUN0001-XYZ", "RUN0001-XYZ", "RUN0001-XYZ"],
                                         "undersoegelsestype": ["virus-isolat", "virus-isolat", "virus-isolat",
                                                                "virus-isolat"],
                                         "ejer": ["KMA", "KMA", "KMA", "KMA"],
                                         "sekventeringsdato": ["2021-11-01", "2021-11-01",
                                                               "2021-11-01",  "2021-11-01"],
                                         "initialer_start": ["XYZ", "XYZ", "XYZ", "XYZ"],
                                         "platform": ["MinION", "MinION",  "MinION",  "MinION"],
                                         "protokol": ["V4", "V4", "V4", "V4"],
                                         "flowcelle": ["FAQ00000", "FAQ00000", "FAQ00000", "FAQ00000"],
                                         "runsheet": [str(self.success_runsheet_path), str(self.success_runsheet_path),
                                                      str(self.success_runsheet_path), str(self.success_runsheet_path)],
                                         "outdata": [str(self.rawdata_path), str(self.rawdata_path),
                                                     str(self.rawdata_path), str(self.rawdata_path)]})
        test_result = parse_nanopore_runsheet.create_metadata_from_runsheet(self.success_runsheet_path,
                                                                            self.rawdata_path, "V4", "virus-isolat",
                                                                            "MinION", self.fake_mads)
        pd.testing.assert_frame_equal(true_result, test_result, check_like=True)