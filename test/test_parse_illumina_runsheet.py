import pathlib
import re
import unittest

from datetime import datetime

import pandas as pd
import pytest

import parse_illumina_runsheet


class TestSplitRunsheet(unittest.TestCase):
    def test_split_simple_sheet(self):
        test_sheet = pathlib.Path(__file__).parent / "data" / "illumina_runsheet" \
                     / "simple_runsheet.csv"
        true_result = {"Header": "IEMFileVersion,4\n"
                                  "Investigator Name,XYZ\n"
                                  "Project Name,testproject\n"
                                  "Experiment Name,test_experiment\n"
                                  "Date,20990101\n"
                                  "Workflow,Generate FASTQ\n"
                                  "Application,\n"
                                  "Assay,Nextera XT\n"
                                  "Description,\n"
                                  "Chemistry,Amplicon\n",
                       "Data": "Sample_ID,I7_Index_ID,index,I5_Index_ID,index2\n"
                               "X99123456-1,N701,TAAGGCGA,S501,TAGATCGC"}
        test_result = parse_illumina_runsheet.split_illumina_sheet(test_sheet)
        assert test_result == true_result

    def test_split_missing_section(self):
        test_sheet = pathlib.Path(__file__).parent / "data" / "illumina_runsheet" \
                     / "runsheet_no_header.csv"
        with pytest.raises(ValueError, match = re.escape("Section(s) {'Header'} were not found in the runsheet.")):
            parse_illumina_runsheet.split_illumina_sheet(test_sheet)


class TestMakeMetadata(unittest.TestCase):
    def setUp(self) -> None:
        self.test_sheet = pathlib.Path(__file__).parent / "data" / "illumina_runsheet" \
                 / "simple_runsheet.csv"
        self.rawdata = pathlib.Path(__file__).parent / "data" / "illumina_runsheet" / "rawdata_dir"
    def test_get_metadata_defaults(self):
        true_result = pd.DataFrame(data={"proevenr": ["X99123456"],
                                         "barcode": ["N701_S501"],
                                         "isolatnr": ["1"],
                                         "koersel": ["test_experiment"],
                                         "undersoegelsestype": ["bakterie-isolat"],
                                         "ejer": ["KMA"],
                                         "sekventeringsdato": [datetime(2099,1,1)],
                                         "initialer_start": ["XYZ"],
                                         "platform": ["MiSeq"],
                                         "protokol": ["Nextera XT"],
                                         "flowcelle": [""],
                                         "runsheet": [str(self.test_sheet)],
                                         "outdata": [str(self.rawdata)]
                                         })
        test_result = parse_illumina_runsheet.get_illumina_metadata(self.test_sheet, self.rawdata,
                                                                    "bakterie-isolat")
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_get_metadata_change_defaults(self):
        true_result = pd.DataFrame(data = {"proevenr": ["X99123456"],
                                           "barcode": ["N701_S501"],
                                           "isolatnr": ["1"],
                                           "koersel": ["test_experiment"],
                                           "undersoegelsestype": ["bakterie-isolat"],
                                           "ejer": ["ABC"],
                                           "sekventeringsdato": [datetime(2099, 1, 1)],
                                           "initialer_start": ["XYZ"],
                                           "platform": ["NextSeq"],
                                           "protokol": ["Nextera XT"],
                                           "flowcelle": [""],
                                           "runsheet": [str(self.test_sheet)],
                                           "outdata": [str(self.rawdata)]
                                           })
        test_result = parse_illumina_runsheet.get_illumina_metadata(self.test_sheet, self.rawdata,
                                                                    "bakterie-isolat",
                                                                    project_owner = "ABC",
                                                                    sequencing_platform = "NextSeq")
        pd.testing.assert_frame_equal(test_result, true_result)