import re
import unittest

import pytest

import check_config

class TestValidateConfig(unittest.TestCase):
    test_config = {"sample_number_settings": {"sample_numbers_in": "number",
                                              "sample_numbers_out": "letter"},
                   "primer_version": "4"}

    def test_fail_sample_numbers(self):
        wrong_sample_numbers = self.test_config
        wrong_sample_numbers["sample_number_settings"]["sample_numbers_in"] = "int"
        wrong_sample_numbers["sample_number_settings"]["sample_numbers_out"] = "string"
        error_message = """The following issue(s) were detected with the config file:
Format of sample numbers used as input must be given as 'number' or 'letter'.
Desired format of sample numbers in output must be given as 'number' or 'letter'."""
        with pytest.raises(ValueError, match=re.escape(error_message)):
            check_config.check_config(wrong_sample_numbers)

    def test_fail_primers(self):
        wrong_primers = self.test_config
        wrong_primers["primer_version"] = "ARTIC v3"
        error_message = """The following issue(s) were detected with the config file:
Invalid primer scheme ARTIC v3. Primer scheme must be one of ['1200', '1200.3', '3', '4', '4.1']."""
        with pytest.raises(ValueError, match=re.escape(error_message)):
            check_config.check_config(wrong_primers)