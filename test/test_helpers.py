import pathlib
import re
import unittest

import pytest

import helpers

# imported from SARS pipeline

class TestMappers(unittest.TestCase):
    def test_number_to_letter(self):
        sample_number = "7099000001"
        sample_letter = "P99000001"
        sample_letter_test = re.sub("^70", helpers.map_sample_number_to_letter, sample_number)
        assert sample_letter == sample_letter_test

    def test_number_not_mapped(self):
        sample_number = "8099000001"
        sample_number_test = re.sub("^80", helpers.map_sample_number_to_letter, sample_number)
        assert sample_number == sample_number_test

    def test_letter_to_number(self):
        sample_number = "7099000001"
        sample_letter = "P99000001"
        sample_number_test = re.sub("^P", helpers.map_sample_letter_to_number, sample_letter)
        assert sample_number == sample_number_test

    def test_letter_not_mapped(self):
        sample_letter = "X99000001"
        sample_letter_test = re.sub("^X", helpers.map_sample_letter_to_number, sample_letter)
        assert sample_letter == sample_letter_test