"""Parse Illumina runsheet and add to metadata database."""

__author__ = "Kat Steinke"

import pathlib

from argparse import ArgumentParser
from datetime import datetime
from io import StringIO
from typing import Dict

import pandas as pd
import yaml

import pipeline_config
from check_config import check_config
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

# split Illumina sheet into sections
def split_illumina_sheet(raw_illumina_sheet: pathlib.Path) -> Dict[str, str]:
    """Split an Illumina runsheet into its individual sections.
    Arguments:
        raw_illumina_sheet: Path to the Illumina runsheet
    Returns:
        The individual sections of the runsheet under their respective headings.
    """
    with open(raw_illumina_sheet, "r", encoding="utf-8") as raw_sheet:
        sheet_sections = {}
        current_header = False
        # read every line in the file
        for line in raw_sheet:
            # header lines are marked with square brackets
            if line.startswith("["):
                current_header = line.strip().replace("[", "").replace("]", "")  # if importing re anyway, make nicer
            else:
                # only start recording when a header has been recorded
                if current_header:
                    if current_header in sheet_sections:
                        sheet_sections[current_header] += line
                    else:
                        sheet_sections[current_header] = line
    # check that all relevant sections are there
    required_headers = {"Header", "Data"}
    if not required_headers.issubset(set(sheet_sections.keys())):
        missing_sections = required_headers - set(sheet_sections.keys())
        raise ValueError(f"Section(s) {missing_sections} were not found in the runsheet.")
    return sheet_sections


# extract information from the relevant sections
# TODO: where to get protocol from? Does the Assay field work for this?
def get_illumina_metadata(runsheet: pathlib.Path, data_dir: pathlib.Path, sequencing_type: str,
                          sequencing_platform: str = "MiSeq", project_owner: str = "KMA") -> pd.DataFrame:
    """Extract information for metadata database from relevant runsheet sections.
    Arguments:
        runsheet:               path to runsheet
        data_dir:               path where sequencing data is saved
        sequencing_type:        type of sequencing run (isolate, 16S, metagenomics)
        sequencing_platform:    sequencing platform (default: MiSeq)
        project_owner:          project owner (KMA for routine runs, researcher otherwise)
    Returns:
        A dataframe with standardized metadata for each sample
    """
    sheet_sections = split_illumina_sheet(runsheet)
    # remove superfluous newline
    sheet_header = sheet_sections["Header"].strip()
    sheet_data = sheet_sections["Data"]
    # read the samples first
    sheet_data_input = StringIO(sheet_data)
    sheet_data_frame = pd.read_csv(sheet_data_input)
    metadata = sheet_data_frame["Sample_ID"].str.extract(r"(?P<proevenr>[A-Z][0-9]{8})-(?P<isolatnr>[0-9])")
    metadata["barcode"] = sheet_data_frame["I7_Index_ID"] + "_" + sheet_data_frame["I5_Index_ID"]
    # header information is easiest to add to everything in one go
    all_header_info = sheet_header.split("\n")
    header_values = {}
    # header rows are comma-separated; heading comes first, followed by value
    for header_row in all_header_info:
        header_row = header_row.split(",")
        header_values[header_row[0]] = header_row[1]
    metadata["koersel"] = header_values["Experiment Name"]
    metadata["undersoegelsestype"] = sequencing_type
    metadata["ejer"] = project_owner
    metadata["sekventeringsdato"] = datetime.strptime(header_values["Date"], "%Y%m%d")
    metadata["initialer_start"] = header_values["Investigator Name"]
    metadata["platform"] = sequencing_platform
    metadata["protokol"] = header_values["Assay"] # TODO: does this make sense?
    metadata["runsheet"] = str(runsheet)
    metadata["outdata"] = str(data_dir) # TODO: can we get this from elsewhere?
    metadata["flowcelle"] = ""
    return metadata[["proevenr", "barcode", "isolatnr", "koersel", "undersoegelsestype", "ejer",
                     "sekventeringsdato","initialer_start", "platform", "protokol", "flowcelle",
                     "runsheet", "outdata"]]




# write to metadata database
if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Extract metadata from runsheet and append to "
                                            "metadata file")
    arg_parser.add_argument("runsheet", help="Path to Illumina runsheet")
    arg_parser.add_argument("sequencing_data", help="Path to folder with sequencing raw data")
    arg_parser.add_argument("database_file", help="Spreadsheet to append results to")
    # TODO: improve - maintain list of sequencing types and protocols and check if matches
    arg_parser.add_argument("--sample_type",
                            help="Type of sample sequenced in sequencing run (e.g 16S)",
                            default="16S")
    arg_parser.add_argument("--platform",
                            help=f"Sequencing platform (default: {workflow_config['platform']})",
                            default=workflow_config["platform"])
    arg_parser.add_argument("--config_file", help="Configuration file for workflow")
    args = arg_parser.parse_args()
    run_sheet = pathlib.Path(args.runsheet)
    sequencing_data = pathlib.Path(args.sequencing_data)
    metadata_file = pathlib.Path(args.database_file)
    sample_type = args.sample_type
    if args.config_file:
        workflow_config_file = pathlib.Path(args.config_file)
        with open(workflow_config_file, "r", encoding = "utf-8") as new_config:
            workflow_config = yaml.safe_load(new_config)
        check_config(workflow_config)
    # platform can be overridden manually
    if not args.platform:
        platform = workflow_config["platform"]
    else:
        platform = args.platform
    # check if everything exists
    if not run_sheet.exists():
        raise FileNotFoundError("Runsheet not found.")
    if not sequencing_data.exists():
        raise FileNotFoundError("Directory with sequencing data not found.")
    if not metadata_file.exists():
        raise FileNotFoundError("Metadata spreadsheet not found.")

    # establish project owner
    owner = workflow_config["project_owner"]

    # for now: load the entire thing into memory and overwrite - TODO: improve
    metadata_old = pd.read_excel(metadata_file)
    metadata_from_runsheet = get_illumina_metadata(run_sheet, sequencing_data, sample_type,
                                                   platform)
    metadata_merged = pd.concat([metadata_old, metadata_from_runsheet], ignore_index=True)
    metadata_merged.to_excel(metadata_file, index=False)